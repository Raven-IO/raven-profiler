
Raven_profiler
===========

A Helm chart for raven agent


## Configuration

The following table lists the configurable parameters of the Raven_profiler chart and their default values.

| Parameter                | Description             | Default        |
| ------------------------ | ----------------------- | -------------- |
| `image.repository` |  | `"726055948281.dkr.ecr.us-east-1.amazonaws.com/raven_profiler"` |
| `image.pullPolicy` |  | `"Always"` |
| `image.tag` |  | `"latest"` |
| `image.digest` |  | `""` |
| `podAnnotations` |  | `{}` |
| `rbac.create` |  | `true` |
| `serviceAccount.create` |  | `true` |
| `serviceAccount.annotations` |  | `{}` |
| `serviceAccount.name` |  | `""` |
| `podSecurityContext.seccompProfile.type` |  | `"RuntimeDefault"` |
| `securityContext.allowPrivilegeEscalation` |  | `true` |
| `securityContext.capabilities.add` |  | `["SYS_ADMIN"]` |
| `securityContext.privileged` |  | `true` |
| `resources.limits.memory` |  | `"600Mi"` |
| `resources.requests.memory` |  | `"100Mi"` |
| `nodeSelector.kubernetes.io/os` |  | `"linux"` |
| `tolerations` |  | `[]` |
| `affinity` |  | `{}` |
| `extraVolumeMounts` |  | `[]` |
| `extraVolumes` |  | `[]` |
| `agent.logLevel` |  | `"info"` |
| `agent.nodeName` |  | `"$(NODE_NAME)"` |
| `agent.clusterName` |  | `""` |
| `agent.cloudProvider` |  | `"aws"` |
| `agent.bpf.verbose` |  | `false` |
| `agent.client.token` |  | `""` |
| `agent.upload.enable` |  | `true` |
| `agent.upload.url` |  | `"https://upload.cloud.raven.io"` |
| `agent.upload.batchWriteInterval` |  | `"3m"` |
| `agent.upload.ravenJdkDataFailureDelay` |  | `"120m"` |
| `agent.upload.ravenJdkDataRefreshInterval` |  | `"20h"` |
| `agent.tracee.enable` |  | `true` |
| `agent.tracee.interval` |  | `"20m"` |
| `agent.tracee.duration` |  | `"5s"` |
| `agent.tracee.commFilter` |  | `"python,java"` |
| `agent.tracee.analyzerEventsOnly` |  | `true` |
| `agent.tracee.traceAllEventsFrequency` |  | `7` |
| `agent.datafetcher.enable` |  | `true` |
| `agent.datafetcher.interval` |  | `"1h"` |
| `agent.metrics.gatherInterval` |  | `"3m"` |
| `agent.loguploader.enable` |  | `true` |
| `agent.loguploader.size` |  | `"1000000"` |
| `agent.profiler.duration` |  | `"1m"` |
| `agent.profiler.profileProcessNames` |  | `""` |
| `agent.debuginfo.upload.disable` |  | `false` |
| `agent.debuginfo.matchFiles` |  | `["/bin/java$", "/libjvm\\.so$", "/libjava\\.so$", "/libjli\\.so$", "/python(\\d*(\\.\\d*)?)?$", "/libpython(\\d*(\\.\\d*)?)?\\.so(\\.\\d*(\\.\\d*)?)?$"]` |
| `agent.debuginfo.extraMatchFiles` |  | `[]` |
| `agent.trivy.enable` |  | `false` |
| `agent.extraArgs` |  | `{}` |
| `ecrAuthenticator.email` |  | `"devops@raven.io"` |
| `ecrAuthenticator.authService` |  | `"ecr-access.cloud.raven.io"` |
| `ecrAuthenticator.rbac.create` |  | `true` |
| `ecrAuthenticator.serviceAccount.create` |  | `true` |
| `ecrAuthenticator.serviceAccount.annotations` |  | `{}` |
| `ecrAuthenticator.serviceAccount.name` |  | `"ecr-authenticator"` |



---
_Documentation generated by [Frigate](https://frigate.readthedocs.io)._

