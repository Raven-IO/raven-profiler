{{- define "raven-agent.params" -}}
- /bin/raven-agent
- --http-address=:7071
- --log-level={{ .Values.agent.logLevel }}
{{- if .Values.agent.bpf.verbose }}
- --verbose-bpf-logging
{{- end }}
- --node={{ .Values.agent.nodeName }}
{{- if .Values.agent.upload.enable }}
- --raven-upload-api-url={{ .Values.agent.upload.url }}
- --remote-store-batch-write-interval={{ .Values.agent.upload.batchWriteInterval }}
- --jdk-data-failure-delay={{.Values.agent.upload.ravenJdkDataFailureDelay}}
- --jdk-data-refresh-interval={{.Values.agent.upload.ravenJdkDataRefreshInterval}}
{{- end }}
- --profiling-duration={{ .Values.agent.profiler.duration }}
{{- if .Values.agent.profiler.profileProcessNames }}
- --debug-process-names={{ .Values.agent.profiler.profileProcessNames }}
{{- end }}
- --debuginfo-temp-dir=/tmp
- --debuginfo-upload-cache-duration=5m
- --debuginfo-upload-timeout-duration=2m
- --analytics-opt-out

{{- if .Values.agent.tracee.enable }}
- --enable-tracee
- --tracee-sample-interval={{ .Values.agent.tracee.interval }}
- --tracee-sample-duration={{ .Values.agent.tracee.duration }}
- --tracee-comm-filter={{ .Values.agent.tracee.commFilter }}
- --tracee-events-to-trace={{- if .Values.agent.tracee.analyzerEventsOnly }}raven_analyzer{{- else }}raven_extra{{- end }}
- --trace-all-events-frequency={{ .Values.agent.tracee.traceAllEventsFrequency }}
{{- end }}

{{- if .Values.agent.trivy.enable }}
- --enable-trivy
{{- end }}

{{- if .Values.agent.datafetcher.enable }}
- --enable-data-fetcher
- --data-fetcher-interval={{ .Values.agent.datafetcher.interval }}
{{- end }}

{{- if .Values.agent.loguploader.enable }}
- --enable-log-uploader
- --log-upload-size={{ .Values.agent.loguploader.size }}
{{- end }}

{{- if .Values.agent.javaClassFilter }}
- --exclude-java-class-names
- {{ join "," .Values.agent.javaClassFilter | quote }}
{{- end }}

- --debuginfo-upload-disable={{- if .Values.agent.debuginfo.upload.disable }}true{{- else }}false{{- end }}
- --debuginfo-match-files
- {{ join "," (concat .Values.agent.debuginfo.matchFiles .Values.agent.debuginfo.extraMatchFiles) | quote }}
- --metrics-gather-interval={{ .Values.agent.metrics.gatherInterval }}

{{- range $key, $value := .Values.agent.extraArgs }}
{{- /* Accept keys without values or with false as value */}}
{{- if eq ($value | quote | len) 2 }}
- --{{ $key }}
{{- else }}
- --{{ $key }}={{ $value }}
{{- end }}
{{- end }}

{{- end }}
